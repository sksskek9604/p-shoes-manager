package com.amh.pshoesmanagaer822.service;

import com.amh.pshoesmanagaer822.entity.Shoes;
import com.amh.pshoesmanagaer822.exception.CMissingDataException;
import com.amh.pshoesmanagaer822.model.ListResult;
import com.amh.pshoesmanagaer822.model.ShoesItem;
import com.amh.pshoesmanagaer822.model.ShoesRequest;
import com.amh.pshoesmanagaer822.repository.ShoesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoesService {

    private final ShoesRepository shoesRepository;

    public void setShoes(ShoesRequest shoesRequest) {
        Shoes shoes = new Shoes.ShoesBuilder(shoesRequest).build();
        shoesRepository.save(shoes);
    }

    public ShoesItem getShoes(long id) {
        Shoes shoes = shoesRepository.findById(id).orElseThrow(CMissingDataException::new);
        ShoesItem result = new ShoesItem.ShoesItemBuilder(shoes).build();

        return result;
    }

    public ListResult<ShoesItem> getShoesAll() {
        List<ShoesItem> result = new LinkedList<>();

        List<Shoes> shoes = shoesRepository.findAll();

        for (Shoes shoes1 : shoes) {
            ShoesItem addItem = new ShoesItem.ShoesItemBuilder(shoes1).build();
            result.add(addItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putShoes(long id, ShoesRequest shoesRequest) {
        Shoes shoes = shoesRepository.findById(id).orElseThrow(ConcurrentModificationException::new);
        shoes.putShoes(shoesRequest);
        shoesRepository.save(shoes);
    }

    public void delShoes(long id) {
        shoesRepository.deleteById(id);
    }

}
