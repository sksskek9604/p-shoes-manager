package com.amh.pshoesmanagaer822.controller;

import com.amh.pshoesmanagaer822.entity.Shoes;
import com.amh.pshoesmanagaer822.model.*;
import com.amh.pshoesmanagaer822.service.ResponseService;
import com.amh.pshoesmanagaer822.service.ShoesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "신발 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/shoes")
public class ShoesController {

    private final ShoesService shoesService;

    @ApiOperation(value = "신발 정보 등록")
    @PostMapping("/new")
    public CommonResult setShoes(@RequestBody @Valid ShoesRequest shoesRequest) {
        shoesService.setShoes(shoesRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "신발 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "신발 시퀀스", required = true)})@GetMapping("/{id}")
    public SingleResult<ShoesItem> getShoes(@PathVariable long id) {
        return ResponseService.getSingleResult(shoesService.getShoes(id));
    }

    @ApiOperation(value = "신발 정보 리스트")
    @GetMapping("/all")
    public ListResult<ShoesItem> getShoesAll() {
        return ResponseService.getListResult(shoesService.getShoesAll(), true);
    }

    @ApiOperation(value = "신발 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "신발 시퀀스", required = true)})
    @PutMapping("/{id}")
    public CommonResult putShoes(@PathVariable long id, @RequestBody @Valid ShoesRequest request) {
        shoesService.putShoes(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "신발 정보 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "신발 시퀀스", required = true)})@DeleteMapping("/{id}")
    public CommonResult delShoes(@PathVariable long id) {
        shoesService.delShoes(id);
        return ResponseService.getSuccessResult();
    }
}
