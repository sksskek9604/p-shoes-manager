package com.amh.pshoesmanagaer822;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PShoesManagaer822Application {

    public static void main(String[] args) {
        SpringApplication.run(PShoesManagaer822Application.class, args);
    }

}
