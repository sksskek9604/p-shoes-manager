package com.amh.pshoesmanagaer822.repository;

import com.amh.pshoesmanagaer822.entity.Shoes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoesRepository extends JpaRepository<Shoes, Long> {
}
