package com.amh.pshoesmanagaer822.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
