package com.amh.pshoesmanagaer822.model;

import com.amh.pshoesmanagaer822.entity.Shoes;
import com.amh.pshoesmanagaer822.enums.ShoesBrand;
import com.amh.pshoesmanagaer822.enums.ShoesOwner;
import com.amh.pshoesmanagaer822.enums.ShoesType;
import com.amh.pshoesmanagaer822.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ShoesItem {

    @ApiModelProperty(notes = "구두 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "구두 별칭 + 구두 주인")
    private String fullName;

    @ApiModelProperty(notes = "구두 가격")
    private Double price;

    @ApiModelProperty(notes = "구두 분류")
    private String shoesType;

    @ApiModelProperty(notes = "구두 브랜드")
    private String shoesBrand;

    @ApiModelProperty(notes = "등록날짜")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정날짜")
    private LocalDateTime dateUpdate;

    private ShoesItem(ShoesItemBuilder shoesItemBuilder) {
        this.id = shoesItemBuilder.id;
        this.fullName = shoesItemBuilder.fullName;
        this.price = shoesItemBuilder.price;
        this.shoesType = shoesItemBuilder.shoesType;
        this.shoesBrand = shoesItemBuilder.shoesBrand;
        this.dateCreate = shoesItemBuilder.dateCreate;
        this.dateUpdate = shoesItemBuilder.dateUpdate;
    }

    public static class ShoesItemBuilder implements CommonModelBuilder<ShoesItem> {

        private final Long id;
        private final String fullName;
        private final Double price;
        private final String shoesType;
        private final String shoesBrand;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ShoesItemBuilder(Shoes shoes) {
            this.id = shoes.getId();
            this.fullName = shoes.getNameShoes() + " " + "주인:" + shoes.getShoesOwner();
            this.price = shoes.getPrice();
            this.shoesType = shoes.getShoesType().getName();
            this.shoesBrand = shoes.getShoesBrand().getName();
            this.dateCreate = shoes.getDateCreate();
            this.dateUpdate = shoes.getDateUpdate();
        }

        @Override
        public ShoesItem build() {
            return new ShoesItem(this);
        }
    }
}
