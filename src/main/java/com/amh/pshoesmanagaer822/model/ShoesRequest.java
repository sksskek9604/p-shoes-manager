package com.amh.pshoesmanagaer822.model;

import com.amh.pshoesmanagaer822.enums.ShoesBrand;
import com.amh.pshoesmanagaer822.enums.ShoesOwner;
import com.amh.pshoesmanagaer822.enums.ShoesType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ShoesRequest {

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "구두별칭", required = true)
    private String nameShoes;

    @NotNull
    @ApiModelProperty(notes = "가격", required = true)
    private Double price;

    @NotNull
    @ApiModelProperty(notes = "구두 분류", required = true)
    private ShoesType shoesType;

    @NotNull
    @ApiModelProperty(notes = "구두 브랜드", required = true)
    private ShoesBrand shoesBrand;

    @NotNull
    @ApiModelProperty(notes = "구두 주인", required = true)
    private ShoesOwner shoesOwner;

}
