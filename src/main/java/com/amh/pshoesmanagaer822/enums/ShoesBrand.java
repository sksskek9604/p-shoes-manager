package com.amh.pshoesmanagaer822.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ShoesBrand {
    NIKE("나이키"),
    ADIDAS("아디다스"),
    VANS("반스"),
    ROADSHOP("로드샵"),
    ONLINE("온라인몰");

    private final String name;
}
