package com.amh.pshoesmanagaer822.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ShoesType {
    ATHLETIC("운동화"),
    SNEAKERS("스니커즈"),
    HEELS("하이힐"),
    ELEVATOR("구두"),
    BOOTS("부츠와 워커"),
    FLIPFLOP("샌들"),
    SLIPPERS("슬리퍼와 실내화");

    private final String name;
}
