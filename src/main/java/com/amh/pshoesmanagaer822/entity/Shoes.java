package com.amh.pshoesmanagaer822.entity;

import com.amh.pshoesmanagaer822.enums.ShoesBrand;
import com.amh.pshoesmanagaer822.enums.ShoesOwner;
import com.amh.pshoesmanagaer822.enums.ShoesType;
import com.amh.pshoesmanagaer822.interfaces.CommonModelBuilder;
import com.amh.pshoesmanagaer822.model.ShoesRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Shoes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String nameShoes;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ShoesType shoesType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ShoesBrand shoesBrand;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ShoesOwner shoesOwner;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    public void putShoes(ShoesRequest request) {
        this.nameShoes = request.getNameShoes();
        this.price = request.getPrice();
        this.shoesType = request.getShoesType();
        this.shoesBrand = request.getShoesBrand();
        this.shoesOwner = request.getShoesOwner();
        this.dateUpdate = LocalDateTime.now();

    }
    private Shoes(ShoesBuilder shoesBuilder) {
        this.nameShoes = shoesBuilder.nameShoes;
        this.price = shoesBuilder.price;
        this.shoesType = shoesBuilder.shoesType;
        this.shoesBrand = shoesBuilder.shoesBrand;
        this.shoesOwner = shoesBuilder.shoesOwner;
        this.dateCreate = shoesBuilder.dateCreate;
        this.dateUpdate = shoesBuilder.dateUpdate;

    }
    public static class ShoesBuilder implements CommonModelBuilder<Shoes> {

        private final String nameShoes;
        private final Double price;
        private final ShoesType shoesType;
        private final ShoesBrand shoesBrand;
        private final ShoesOwner shoesOwner;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public ShoesBuilder(ShoesRequest request) {
            this.nameShoes = request.getNameShoes();
            this.price = request.getPrice();
            this.shoesType = request.getShoesType();
            this.shoesBrand = request.getShoesBrand();
            this.shoesOwner = request.getShoesOwner();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Shoes build() {
            return new Shoes(this);
        }
    }



}
